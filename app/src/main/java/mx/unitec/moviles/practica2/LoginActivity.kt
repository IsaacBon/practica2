package mx.unitec.moviles.practica2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class LoginActivity : AppCompatActivity() {

    private lateinit var editTextTextEmailAddress: EditText
    private lateinit var editTextTextPassword: EditText
    private lateinit var button: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        editTextTextEmailAddress = findViewById(R.id.editTextTextEmailAddress)
        editTextTextPassword = findViewById(R.id.editTextTextPassword)
        button =findViewById(R.id.button)

        button.setOnClickListener {

            if(editTextTextEmailAddress.text.isEmpty())

                editTextTextEmailAddress.error = getString(R.string.error_email)

            if(editTextTextPassword.text.isEmpty())

                editTextTextPassword.error = getString(R.string.error_password)

            Toast.makeText(this, R.string.welcome_message, Toast.LENGTH_SHORT).show()
            Toast.makeText(this, editTextTextEmailAddress.text, Toast.LENGTH_SHORT).show()

        }
    }
}